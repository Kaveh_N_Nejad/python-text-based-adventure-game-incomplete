class Food:
    def __init__(self,Name,Replenishment):
        self.Name=Name
        self.Replenishment=Replenishment
        
class Weapon:
    def __init__(self,Name,Durability,Damage):
        self.Name=Name
        self.Durability=Durability
        self.Damage=Damage
        
class Shield:
    def __init__(self,Name,Durability,Defence):
        self.Name=Name
        self.Durability=Durability
        self.Defence=Defence

class Armor:
    def __init__(self,Name,Durability,Armor_Rating):
	    self.Name=Name
	    self.Durability=Durability
	    self.Armor_Rating=Armor_Rating


Bandit_Shield=Shield("bandit shield",40,3)
Weak_Shield=Shield("weak shield",50,2)
Steel_Shield=Shield("steel shield",100,5)
Wooden_Shield=Shield("wooden shield",15,2)
Knights_Shield=Shield("knights shield",100,6)


Shields=[Bandit_Shield,Shield,Steel_Shield,Wooden_Shield,Weak_Shield,Knights_Shield]
Shields_Dict={"bandit shield":Bandit_Shield,"weak shield":Weak_Shield,"steel shield":Steel_Shield,"wooden shield":Wooden_Shield,
              "knights shield":Knights_Shield}
        
Bandit_Sword=Weapon("bandit sword",12,3)
Battle_Axe=Weapon("battle axe",100,5)
Dagger=Weapon("dagger",70,2)
Sword=Weapon("sword",80,4)
Heavy_Axe=Weapon("heavy axe",70,6)
Battle_Hammer=Weapon("battle hammer",90,5)
Spear=Weapon("spear",18,3)
Pike=Weapon("pike",30,4)
Axe=Weapon("axe",5,3)

Axes=[Battle_Axe,Heavy_Axe,Axe]
Weapons=[Bandit_Sword,Battle_Axe,Dagger,Sword,Heavy_Axe,Battle_Hammer,Spear,Pike,Axe]
Weapons_Dict={"bandit sword":Bandit_Sword,"battle axe":Battle_Axe,"dagger":Dagger,"sword":Sword,"heavy axe":Heavy_Axe,"battle hammer":Battle_Hammer,
              "spear":Spear,"pike":Pike,"axe":Axe}

Apple=Food("apple",2)
Crab_Meat=Food("crab meat",3)
Bread=Food("bread",2)
Carrot=Food("carrot",3)
Chicken=Food("chicken",4)
Wolf_Meat=Food("wolf meat",5)


Foods=[Apple,Crab_Meat,Bread,Carrot,Chicken]
Foods_Dict={"apple":Apple,"crab meat":Crab_Meat,"bread":Bread,"carrot":Carrot,"chicken":Chicken,"wolf meat":Wolf_Meat}

Forest_Items=["wood"]

Dict={}

Dict.update(Weapons_Dict)
Dict.update(Foods_Dict)
Dict.update(Shields_Dict)

