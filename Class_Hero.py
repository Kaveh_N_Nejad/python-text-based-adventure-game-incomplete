import Items
class Hero_Set:
        def __init__(self,Name,Health,Attack,Defence):
                self.Weapon="empty"
                self.Shield="empty"
                self.Armor="empty"
                self.Name = Name
                self.Hand_Defence=Defence
                self.Defence = Defence
                self.Health = Health
                self.Hand_Health = Health
                self.Attack = Attack
                self.Hand_Attack = Attack
                self.Inventory=[Items.Pike,Items.Steel_Shield,"empty","empty","empty","empty","empty","empty"]

                
        def View_Inventory(Hero):
                print ("In your inventory you have:")
                for i in range(1,9):
                        if Hero.Inventory[i-1]!="empty":
                                print (str(i)+")",Hero.Inventory[i-1].Name ,end=" ")
                                if Hero.Inventory[i-1] in Items.Weapons:
                                        print ("   Durability: ",(Hero.Inventory[i-1]).Durability,"Damage: ",(Hero.Inventory[i-1]).Damage)
                                elif Hero.Inventory[i-1] in Items.Shields:
                                        print ("   Durability: ",(Hero.Inventory[i-1]).Durability,"Damage: ",(Hero.Inventory[i-1]).Defence)
                                elif Hero.Inventory[i-1] in Items.Foods:
                                        print ("   Replenishment :",Hero.Inventory[i-1].Replenishment)
                                else:
                                        print ("")
                        else:
                                print (str(i)+")","empty")
                                

        def Heal(Hero):
                Eaten=False
                for i in Hero.Inventory:
                        if i in Items.Foods:
                                Hero.Health+=i.Replenishment
                                print ("You have healed by %d" %i.Replenishment)
                                Hero.Inventory.remove(i)
                                Hero.Inventory.append("empty")
                                Eaten=True
                                break
                if Eaten==False:
                        print ("You dont have any food")
                return Hero


        def Add_To_Inventory(Hero,Item):
                if "empty" in Hero.Inventory:
                        for num,i in enumerate(Hero.Inventory):
                                if i=="empty":
                                        Hero.Inventory[num]=Item
                                        break
                else:
                        print("You do not have any empty room in your inventory if you want to add a new item you need to discard one")
                        Hero_Set.View_Inventory(Hero)
                        Decision=input("Please select weither to discard 'discard' or leave the new item 'leave': ").lower()
                        if Decision=="discard":
                                Discard_Item=input("Please enter a item you want to discard: ").lower()
                                Discard_Item=Items.Dict[Discard_Item]
                                while Discard_Item not in Hero.Inventory:
                                        Discard_Item=input("The item you have entered is not in your invenotey, please enter a item you want to discard: ").lower()
                                Hero.Inventory.remove(Discard_Item)
                                Hero.Inventory.append(Item)
                return Hero


        def Equip(Hero):
                Got_Item=False
                while not Got_Item:
                        Item=input("which item do you want to equip: ").lower()
                        if Item.lower()=="cancel":
                                return Hero
                        elif Item.lower()=="view":
                                Hero_Set.View_Inventory(Hero)
                        if Item in (Items.Dict).keys():
                                Item=Items.Dict[Item]
                                while Item not in Hero.Inventory:
                                        Item=input("That item is not in your inventory,if you want to view your inventory enter 'view',\nwhich item do you want to equip, enter 'cancel' to cancel: ").lower()
                                        if Item.lower()=="cancel":
                                                return Hero
                                        elif Item.lower()=="view":
                                                Hero_Set.View_Inventory(Hero)
                                        if Item in Items.Dict:
                                                Item=Items.Dict[Item]
                                Got_Item=True
                        else:
                                print("That is not an item you can equip")

                if Item in Items.Shields:
                        if Hero.Shield=="empty":
                                Hero.Shield=Item
                                Hero.Defence=Hero.Hand_Defence
                                Hero.Defence+=Item.Defence
                                Hero.Inventory.remove(Item)
                                Hero.Inventory.append("empty")
                        else:
                                print ("You already have a Shield")
                                print (Hero.Shield.Name,"Defence",Hero.Shield.Defence,"durability",Hero.Shield.Durability)
                                Choice=input("Do you wish to discard it? 'yes' or 'no' :").lower()
                                while Choice!="no" and Choice !="yes":
                                        Choice=input("Do you wish to discard it? 'yes' or 'no' :").lower()
                                if Choice=="yes":
                                        for i in range(8):
                                                if i==Item:
                                                        Hero.Inventory[i],Hero.Shield=Hero.Shield,Hero.Inventory[i]
                                                        Hero.Defence=Hero.Hand_Defence
                                                        Hero.Defence+=Item.Damage
                                                        break
                elif Item in Items.Weapons:     
                        if Hero.Weapon=="empty":
                                Hero.Weapon=Item
                                Hero.Attack=Hero.Hand_Attack
                                Hero.Attack+=Item.Damage
                                Hero.Inventory.remove(Item)
                                Hero.Inventory.append("empty")
                        else:
                                print ("You already have a weapon")
                                print (Hero.Weapon.Name,"attack",Hero.Weapon.Damage,"durability",Hero.Weapon.Durability)
                                Choice=input("Do you wish to discard it? 'yes' or 'no' :").lower()
                                while Choice!="no" and Choice !="yes":
                                        Choice=input("Do you wish to discard it? 'yes' or 'no' :").lower()
                                if Choice=="yes":
                                        for i in range(8):
                                                if i==Item:
                                                        Hero.Inventory[i],Hero.Weapon=Hero.Weapon,Hero.Inventory[i]
                                                        Hero.Attack=Hero.Hand_Attack
                                                        Hero.Attack+=Item.Damage
                                                        break


                
                return Hero

                        

        def View_Stats(Hero):
                print ("Your stats are:")
                if Hero.Weapon!="empty":
                        print ("Weapon: ",(Hero.Weapon).Name,"Durability:",(Hero.Weapon).Durability,"Attack: ",(Hero.Weapon).Damage)
                else:
                        print ("Weapon : empty")
                if Hero.Shield != "empty":
                        print ("Shield: ",(Hero.Shield).Name,"Durability:",(Hero.Shield).Durability,"Defence: ",(Hero.Shield).Defence)
                else:
                        print ("Shield : empty")
                print ("Health: ",Hero.Health,"\nAttack: ",Hero.Attack,"\nDefence: ",Hero.Defence)
                       
