from random import randint
import Create_Enemy
import Class_Hero
Actions=['flee',"attack","view inventory","view stats","heal","view my stats"]

def Main(Hero,Enemy):
    Battle_State="hero turn"
    print ("You have been attacked by a",Enemy.Name)
    Create_Enemy.Enemy.Get_Stats(Enemy)
    while Hero.Health>0 and Enemy.Health>0 and Battle_State!="over":
        Action=Get_Action()
        
        if Action=="flee":
            Battle_State=Flee()
            if Battle_State=="over":
                return Hero
            Battle_State="enemy turn"

        elif Action=="attack":
            Damage=Hero.Attack-Enemy.Defence
            if Hero.Weapon !="empty":
                (Hero.Weapon).Durability-=Damage/3
            if Damage>0:
            	Enemy.Health-=Damage
            	print("The enemy took",Damage,"damage")
            else :
            	print("The enemy didnt take any damage")
            Battle_State="enemy turn"
            

        elif Action=="view stats":
            Create_Enemy.Enemy.Get_Stats(Enemy)

        elif Action=="view inventory":
            Class_Hero.Hero_Set.View_Inventory(Hero)

        elif Action=="heal":
            Health=Hero.Health
            Hero=Class_Hero.Hero_Set.Heal(Hero)
            if Hero.Health>Health:
                Battle_State="enemy turn"

        elif Action=="view my stats":
            Class_Hero.Hero_Set.View_Stats(Hero)


        if Enemy.Health<=0:
            print ("You have emerged victorius from the battle")
            Hero=Class_Hero.Hero_Set.Add_To_Inventory(Hero,Enemy.Reward)
            print ("Your reward is :",Enemy.Reward.Name)
        else:
            if Battle_State=="enemy turn":
                Damage=Enemy.Attack-Hero.Defence
                if Hero.Shield !="empty":
                    (Hero.Shield).Durability-=Damage/3
                
                if Damage>0:
                    Hero.Health-=Damage
                    print ("You took %d Damage"%(Damage))
                else:
                    print ("You didnt take any damage")
                Battle_State="hero turn"
        
                 
    return Hero
    

def Get_Action():
    Action=input("Enter a action: ").lower()
    while Action not in Actions:
        print ("The actions you can take are: ")
        for num,i in enumerate(Actions,1):
            print (str(num)+")",i)
        Action=input("Enter a action: ").lower()
    return Action


    
def Flee():
    Rannum=randint(0,10)
    if Rannum>5:
        print ("You suceded")
        return "over"
    else:
        print ("You didnt manage to flee")
        return "ongoing"



