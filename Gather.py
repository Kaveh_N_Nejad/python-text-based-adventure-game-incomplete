import Items
def Gather(Hero,Map_Type):
    Has_Item=False
    if Map_Type=="Forest":
        Gather_Item=Get_Gather_Item(Map_Type)
        if Gather_Item=="wood":
            if Hero.Weapon in Items.Axes:
                Has_Item=True
            else:
                for Item in Hero.Inventory:
                    if Item in Items.Axes:
                        Has_Item=True
                        break
            if Has_Item==True:
                Hero=Hero.Add_To_Inventory(Hero,"wood")
            else:
                print ("You need a axe")

    return Hero

def Get_Gather_Item(Map_Type):
    Gather_Item=input("Enter the item you want to gather").lower()
    while Gather_Item not in eval(Items.Map_Type._Items):
        print ("You cannot gaver that here. The Items you can gather in a forest are:")
        for num,i in enumerate(Items.Forest_Items):
            print (str(num)+")",i)
        Gather_Item=input("Enter the item you want to gather").lower()
