from Set_Up import *
import Move
import Spawn_Enemy
import My_Map
import Buildings
import Battle
import Class_Hero
import copy
import Gather

Actions=["move","action help","view map","fix","view inventory","stay","heal","view my stats","equip","gather"]
def Get_Action():
    Action=input("What do you want to do:  ").lower()
    while Action not in Actions:
        print ("The action you entered is incorect")
        print ("if you want a list of posible actions please enter 'action help'")
        Action=input("What do you want to do:  ").lower()
    return Action
 
My_Map.Add_To_Map(Location,Map[Location])
Map[Buildings.Boatyard.Location]="Boatyard"
Turn="done"

while Hero.Health>0:
    if Turn=="done":
        print ("You are in ",Map[Location],"\nTo your north is ",Map[Location-(Set_Up_X+2)],"  To your east is ",Map[Location+1])
        print ("To your south is ",Map[Location+(Set_Up_X+2)],"  To your west is ",Map[Location-1])
    Turn="ongoing"
    Action=Get_Action()
    if Action=="move":
        Result=(Move.Move(input("Please enter a direction:  "),Location))
        if Result !="cancel":
            Location=Result
            My_Map.Add_To_Map(Location,Map[Location])
            Turn="done"

    elif Action=="action help":
        print("The actions you can take are")
        for num,i in enumerate (Actions,1):
            print (str(num)+")",i)
        
    elif Action=="stay":
        Turn="done"

    elif Action=="heal":
        Hero=Class_Hero.Hero_Set.Heal(Hero)
            
    elif Action=="view map":
        My_Map.Get_Map(Location)

    elif Action=="gather":
        Gather.Gather(Hero,Map[Location])

    elif Action=="fix":
        if Map[Location] in Buildings.Buildings_List:
            Buildings.Fix(Hero.Inventory,Map[Location])
            Turn="done"
        else:
            print ("You must be at a building to fix it")

    elif Action=="view inventory":
        Class_Hero.Hero_Set.View_Inventory(Hero)

    elif Action=="equip":
        Class_Hero.Hero_Set.Equip(Hero)

    elif Action=="view my stats":
        Class_Hero.Hero_Set.View_Stats(Hero)

    
        
    if Turn=="done":
        Enemy=copy.copy(Spawn_Enemy.Spawn(Map[Location]))
        if Enemy is not None:
            if Enemy!="null":
                Hero=Battle.Main(Hero,Enemy)
    

    print("\n\n")
print("Game over")

