from Create_Enemy import Plains_Enemys,Swamp_Enemys,Forest_Enemys,Hills_Enemys,Beach_Enemys
from random import randint
def Spawn(Map_Type):
    if Map_Type=="plains":
        return(Plains_Enemys[randint(0,len(Plains_Enemys)-1)])
    elif Map_Type=='swamp':
        return(Swamp_Enemys[randint(0,len(Swamp_Enemys)-1)])
    elif Map_Type=='forest':
        return(Forest_Enemys[randint(0,len(Forest_Enemys)-1)])
    elif Map_Type=='hills':
        return(Hills_Enemys[randint(0,len(Hills_Enemys)-1)])
    elif Map_Type=='beach':
        return(Beach_Enemys[randint(0,len(Beach_Enemys)-1)])
