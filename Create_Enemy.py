import Items
from random import choice
class Enemy:
    def __init__(self,Name,Health,Attack,Defence,Reward):
        self.Name=Name
        self.Health=Health
        self.Attack=Attack
        self.Defence=Defence
        self.Reward=Reward

    def Get_Stats(Enemy):
        print ("The ",Enemy.Name+"'s stats are: \n Health:",Enemy.Health,"\n Attack:",Enemy.Attack,"\n Defence:",Enemy.Defence,"\n Reward:",Enemy.Reward.Name)

#all
Lone_Bandit=Enemy("Lone Bandit",5,3,2,Items.Bandit_Shield)
Bandit=Enemy("Bandit",3,3,2,choice(Items.Foods))
Goblin=Enemy("Goblin",3,3,2,Items.Pike)
Wolf=Enemy("Wolf",2,3,2,Items.Wolf_Meat)

#beach
Crab=Enemy("Crab",1,2,3,Items.Crab_Meat)

#forest
Giant_Ape=Enemy("Giant Ape",6,4,3,Items.Apple)
Goblin_Pack=Enemy("Goblin Pack",5,4,5,Items.Axe)

#hills
Troll=Enemy("Troll",8,6,6,Items.Knights_Shield)
Heavy_Bandit=Enemy("Heavy Bandit",6,6,7,Items.Battle_Axe)


#Swamp
Strong_Bandit=Enemy("Strong bandit",5,4,5,Items.Steel_Shield)

#plains
Plains_Goblin=Enemy("Goblin",3,4,3,Items.Spear)



All=[Lone_Bandit,Wolf,Goblin]

Plains_Enemys=[Plains_Goblin]+All
Swamp_Enemys=[]+All
Forest_Enemys=[Goblin_Pack]+All
Hills_Enemys=[Heavy_Bandit,Troll]+All
Beach_Enemys=[Crab]+All


